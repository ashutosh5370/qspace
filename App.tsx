import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image } from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import HomeScreen from './components/home';
import AboutScreen from './components/about';

export default class App extends React.Component {
  state = {
    email: "",
    password: ""
  }
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={require('../Que/assets/React.webp')}
        />
        <View style={styles.inputView} >
          <TextInput
            style={styles.inputText}
            placeholder="Mobile Number..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ email: text })} />
        </View>
        <View style={styles.inputView} >
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password..."
            placeholderTextColor="#003f5c"
            onChangeText={text => this.setState({ password: text })} />
        </View>

        <TouchableOpacity style={styles.loginBtn}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.SocialBtn}>
          <Text style={styles.SocialTextFB}>Facebook</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.SocialBtn}>
          <Text style={styles.SocialTextGoogle}>Google</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  SocialBtn: {
    borderWidth: 2,
    marginBottom: 10,
    borderRadius: 20,
    width: "60%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderColor: '#3b5998'

  },
  SocialTextFB: {
    color: "#3b5998"

  },
  SocialTextGoogle: {

  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 100,
    height: 100,
    margin: 20
  },
  inputView: {
    width: "70%",
    borderBottomWidth: 1.0,
    height: 50,
    marginBottom: 30,
    justifyContent: "center",
    padding: 20
  },
  inputText: {
    height: 60,
    color: "white"

  },
  forgot: {
    marginTop: 20,
    color: "black",
    fontSize: 13
  },
  loginBtn: {
    width: "60%",
    backgroundColor: "#00a1ff",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 30
  },
  loginText: {
    color: "white"
  }
});